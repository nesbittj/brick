
# BRICK

Grid based game using Firebase database.

## Run

```
npm start
```

## Build and Deploy

```
npm run build
firebase deploy
```