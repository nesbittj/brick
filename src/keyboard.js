let keyboard = {
    map: {
        camera: {
            up: "w",
            left: "a",
            down: "s",
            right: "d",
            reset: "q"
        },
        play: "o",
        pause: "p",
        zoom: {
            in: "=",
            out: "-",
            reset: "z"
        }
    },
    _keyDown: {},
    init: function () {
        document.addEventListener('keydown', function (e) {
            if (!keyboard.keyDown[e.key] || !keyboard.keyDown[e.key].held) {
                keyboard.keyDown[e.key] = {
                    held: true,
                    down: true
                }
            }
        });
        document.addEventListener('keyup', function (e) {
            keyboard.keyDown[e.key] = {
                held: false,
                down: false
            }
        });
    },
    keyDown: function (key) {
        return keyboard.keyDown[key] && keyboard.keyDown[key].down;
    },
    keyOnce: function (key) {
        if (keyboard.keyDown[key] && keyboard.keyDown[key].down) {
            keyboard.keyDown[key].down = false;
            return true;
        }
        return false;
    }
}

export default keyboard;