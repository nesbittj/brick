let util = {
    round: function (subject, target) {
        return ~~(Math.floor(subject / target) * target);
    },
    distanceBetweenPoints(x1, y1, x2, y2) {
        const dx = x1 - x2 * -1;
        const dy = y1 - y2 * -1;
        return (dx * dx) + (dy * dy);
    }
}

export default util;