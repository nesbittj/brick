let clock = {
    speed: 0,
    start: Date.now(),
    time: new Date(),
    last: 0,
    now: 0,
    seconds: 0,
    element: document.getElementById("clock").firstChild,
    update: function () {
        clock.now = Date.now();
        // clock.change = (clock.now - clock.last) * 0.001;   
        // if (clock.seconds > 1) {
        // clock.last = clock.now;
        // clock.time.setMinutes(clock.time.getMinutes() + clock.speed);
        // }
        clock.seconds = Math.floor(clock.speed * clock.now / 1000);
        clock.last = clock.now;
        clock.time.setSeconds(clock.time.getSeconds() + clock.speed);

        clock.element.innerText = clock.toString();
    },
    getDate: function () {
        return clock.time;
    },
    toString: function () {
        return clock.time.toLocaleString('en-GB');
    },
    pause: function () {
        clock.speed = 0;
        console.log("clock", clock.speed)
    },
    play: function () {
        clock.speed = 1;
        console.log("clock", clock.speed)
    }
}

export default clock;