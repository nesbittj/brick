let threat = {
    pos: {
        x: 0,
        y: 0
    },
    speed: 1,
    lastUpdate: 0,
    update: function (nextUpdate, brickSize, target) {
        let provisional = threat.pos;
        let currentStep = nextUpdate > threat.lastUpdate ? brickSize * threat.speed : 0;
        threat.lastUpdate = nextUpdate;
        if (target.x < threat.pos.x) {
            provisional.x -= currentStep;
        } else if (target.x > threat.pos.x) {
            provisional.x += currentStep;
        }
        if (target.y < threat.pos.y) {
            provisional.y -= currentStep;
        } else if (target.y > threat.pos.y) {
            provisional.y += currentStep;
        }
    }
}

export default threat;