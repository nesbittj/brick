import './style.css';
import clock from './clock';
import testPug from '../template/controls.pug';
import threat from './threat';
import keyboard from './keyboard';
import library from './library';
import util from './util';

const BUTTON_LEFT = 0;
const BUTTON_MIDDLE = 1;
const BUTTON_RIGHT = 2;

const colour = {
        GREEN: {
            key: "1",
            val: "green"
        },
        RED: {
            key: "2",
            val: "red"
        },
        GREY: {
            key: "3",
            val: "grey"
        },
        BLUE: {
            key: "4",
            val: "blue"
        },
        ERASER: {
            key: "0",
            val: "eraser"
        }
    },
    brickSize = 40;
let mouse = {
        pos: {
            x: 0,
            y: 0
        },
        down: false,
        downOnce: false,
        lastDownPos: {
            x: 0,
            y: 0
        },
        brick: {
            x: 0,
            y: 0
        }
    },
    selected = {
        colour: colour.GREEN.val
    },
    camera = {
        pos: {
            x: 0,
            y: 0
        },
        down: false,
        lastDownPos: {
            x: 0,
            y: 0
        },
        firstDownPos: {
            x: 0,
            y: 0
        },
        speed: 10,
        scale: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        }
    };

let ctx, worldBricksWidth, worldBricksHeight, worldBricksWidthHalf, worldBricksHeightHalf,
    ctxScale = 1;

function init() {

    var firebaseConfig = {
        apiKey: "AIzaSyBGzj1aLfD7cV5pGDAFcxrrsJZKkZ2BFc8",
        authDomain: "brick-world.firebaseapp.com",
        databaseURL: "https://brick-world.firebaseio.com",
        projectId: "brick-world"
    };

    firebase.initializeApp(firebaseConfig);

    // const canvasGrid = document.getElementById('brick-world-canvas-grid');
    // const ctxGrid = canvasGrid.getContext('2d');
    const canvas = document.getElementById('brick-world-canvas');
    ctx = canvas.getContext('2d');

    worldBricksWidth = canvas.width = canvas.width = window.innerWidth;
    worldBricksHeight = canvas.height = canvas.height = window.innerHeight;
    worldBricksWidthHalf = worldBricksWidth / 2;
    worldBricksHeightHalf = worldBricksHeight / 2;
    console.log("worldBricksWidth", worldBricksWidth);
    console.log("worldBricksHeight", worldBricksHeight);

    //draw grid
    // ctxGrid.lineWidth = 1;
    // ctxGrid.strokeStyle = colour.GREY;
    // for (let i = 0; i < worldBricksWidth; i += brickSize) {
    //   ctxGrid.moveTo(i, 0);
    //   ctxGrid.lineTo(i, worldBricksHeight);
    //   ctxGrid.stroke();
    // }
    // for (let j = 0; j < worldBricksHeight; j += brickSize) {
    //   ctxGrid.moveTo(0, j);
    //   ctxGrid.lineTo(worldBricksWidth, j);
    //   ctxGrid.stroke();
    // }

    library.init(brickSize, ctx, camera);

    camera.scale.right = unScaleWidth(worldBricksWidth);
    camera.scale.bottom = unScaleHeight(worldBricksHeight);
    camera.scale.left = unScaleWidth(brickSize);
    camera.scale.top = unScaleHeight(brickSize);

    //mouse events
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });
    document.addEventListener('mousedown', function (e) {

        //check mouse is over canvas
        const pointElement = document.elementFromPoint(mouse.pos.x, mouse.pos.y);
        if (pointElement.tagName !== 'CANVAS') {
            return;
        }

        switch (e.button) {
            case BUTTON_LEFT:
                if (!mouse.down) {
                    mouse.downOnce = true;
                }
                mouse.down = true;
                break;
            case BUTTON_MIDDLE:
                camera.down = true;
                camera.firstDownPos = {
                    x: e.clientX,
                    y: e.clientY
                };
                break;
            case BUTTON_RIGHT:
                library.deleteBrick(~~(mouse.brick.x), ~~(mouse.brick.y))
                break;
        }
    });
    document.addEventListener('mouseup', function (e) {
        switch (e.button) {
            case BUTTON_LEFT:
                mouse.downOnce = false;
                mouse.down = false;
                break;
            case BUTTON_MIDDLE:
                camera.down = false;
                break;
        }
    });
    document.addEventListener('mousemove', function (e) {
        mouse.pos.x = e.clientX;
        mouse.pos.y = e.clientY;
    });
    document.addEventListener('wheel', function (e) {
        if (e.deltaY < 0) {
            zoomIn();
        } else {
            zoomOut();
        }
    });

    keyboard.init();

    updateInput();

    render();
}

function unScaleWidth(subject) {
    let result = (subject - worldBricksWidthHalf) / ctxScale;
    result -= (camera.pos.x - worldBricksWidthHalf);
    return result;
}

function unScaleHeight(subject) {
    let result = (subject - worldBricksHeightHalf) / ctxScale;
    result -= (camera.pos.y - worldBricksHeightHalf);
    return result;
}

function zoomIn() {
    ctxScale += 0.1;
    if (ctxScale > 2) {
        ctxScale = 2;
    }
}

function zoomOut() {
    ctxScale -= 0.1;
    if (ctxScale < 0.1) {
        ctxScale = 0.1;
    }
}

function zoomReset() {
    ctxScale = 1;
}

function updateInput() {

    //update camera from mouse
    if (camera.down) {
        camera.pos = {
            x: camera.lastDownPos.x - (camera.firstDownPos.x - mouse.pos.x) / ctxScale,
            y: camera.lastDownPos.y - (camera.firstDownPos.y - mouse.pos.y) / ctxScale
        }
        camera.lastDownPos = {
            x: camera.pos.x,
            y: camera.pos.y
        };
        camera.firstDownPos = {
            x: mouse.pos.x,
            y: mouse.pos.y
        };
    }

    //update camera from keyboard
    if (keyboard.keyDown(keyboard.map.camera.reset)) {
        camera.pos.x = camera.pos.y = 0;
    }
    if (keyboard.keyDown(keyboard.map.camera.up)) {
        camera.pos.y += camera.speed / ctxScale;
    }
    if (keyboard.keyDown(keyboard.map.camera.left)) {
        camera.pos.x += camera.speed / ctxScale;
    }
    if (keyboard.keyDown(keyboard.map.camera.down)) {
        camera.pos.y -= camera.speed / ctxScale;
    }
    if (keyboard.keyDown(keyboard.map.camera.right)) {
        camera.pos.x -= camera.speed / ctxScale;
    }

    mouse.brick.x = util.round(unScaleWidth(mouse.pos.x), brickSize);
    mouse.brick.y = util.round(unScaleHeight(mouse.pos.y), brickSize);

    //update input selection
    if (keyboard.keyDown(colour.GREEN.key)) {
        selected.colour = colour.GREEN.val;
    }
    if (keyboard.keyDown(colour.RED.key)) {
        selected.colour = colour.RED.val;
    }
    if (keyboard.keyDown(colour.BLUE.key)) {
        selected.colour = colour.BLUE.val;
    }
    if (keyboard.keyDown(colour.GREY.key)) {
        selected.colour = colour.GREY.val;
    }
    if (keyboard.keyDown(colour.ERASER.key)) {
        selected.colour = colour.ERASER.val;
    }

    if (keyboard.keyOnce(keyboard.map.play)) {
        clock.play();
    }
    if (keyboard.keyOnce(keyboard.map.pause)) {
        clock.pause();
    }


    if (keyboard.keyOnce(keyboard.map.zoom.in)) {
        zoomIn();
    }
    if (keyboard.keyOnce(keyboard.map.zoom.out)) {
        zoomOut();
    }
    if (keyboard.keyOnce(keyboard.map.zoom.reset)) {
        zoomReset();
    }

    if (mouse.down) {
        if (selected.colour === colour.ERASER.val) {
            if (library.getBrick(mouse.brick.x, mouse.brick.y)) {
                library.deleteBrick(mouse.brick.x, mouse.brick.y);
            }
        } else {
            if (!library.getBrick(mouse.brick.x, mouse.brick.y)) {
                library.writeBrick(mouse.brick.x, mouse.brick.y, selected.colour);
            }
        }
    }
    mouse.downOnce = false;

    camera.scale.right = unScaleWidth(worldBricksWidth);
    camera.scale.bottom = unScaleHeight(worldBricksHeight);
    camera.scale.left = unScaleWidth(-brickSize);
    camera.scale.top = unScaleHeight(-brickSize);

    camera.lastDownPos = {
        x: camera.pos.x,
        y: camera.pos.y
    };
}

function render() {
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, worldBricksWidth, worldBricksHeight);
    ctx.setTransform(ctxScale, 0, 0, ctxScale, worldBricksWidthHalf, worldBricksHeightHalf);
    ctx.translate(camera.pos.x - worldBricksWidthHalf, camera.pos.y - worldBricksHeightHalf);

    clock.update();
    threat.update(clock.seconds, brickSize, {
        x: mouse.brick.x,
        y: mouse.brick.y
    });
    if (library.getBrick(threat.pos.x, threat.pos.y)) {
        library.writeBrick(threat.pos.x, threat.pos.y, colour.RED.val);
    }

    updateInput();

    library.update();
    library.render();

    if (debugElem) {
        ctx.fillStyle = 'white';
        ctx.fillRect(520, 520, brickSize, brickSize);
        ctx.fillRect(0, 0, brickSize, brickSize);
    }

    ctx.fillStyle = 'red';
    ctx.fillRect(threat.pos.x, threat.pos.y, brickSize, brickSize);

    //mouse pointer
    ctx.fillStyle = 'orange';
    ctx.fillRect(mouse.brick.x, mouse.brick.y, brickSize, brickSize);

    requestAnimationFrame(render);
}

document.addEventListener('DOMContentLoaded', function () {
    const controlsHTML = testPug({
        dataColour: colour
    });
    document.getElementById("controls").innerHTML = controlsHTML;

    document.getElementById('control-play').onclick = () => {
        clock.play();
    }
    document.getElementById('control-pause').onclick = () => {
        clock.pause();
    }

    window.debugElem = document.getElementById("debug");

    init();
});