import util from './util';

const chunkBatchLength = 3,
    chunkBatchTarget = chunkBatchLength * 20,
    chunkDistanceMax = 4500 * 4500;

let library = {
    camera: null,
    ctx: null,
    brickSize: 32,
    chunkSize: null,
    lastChunkKey: null,
    lastCameraPos: {
        x: 0,
        y: 0
    },
    databaseRef: null,
    chunkMap: {},
    chunksToDelete: {},
    init: function (brickSize, ctx, camera) {
        let databasePath = '/brick_map';
        const envDev = window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1';
        if (envDev) {
            databasePath = '/dev' + databasePath;
        }

        library.brickSize = brickSize
        library.chunkSize = 32 * library.brickSize;
        library.ctx = ctx;
        library.camera = camera;

        library.databaseRef = firebase.database().ref(databasePath);
        library.update();
    },
    update: function () {
        changeChunk();
    },
    render: function () {
        let i = 0,
            j = 0,
            k = 0;
        Object.values(library.chunkMap).forEach((chunk) => {
            k++;
            Object.values(chunk.bricks).forEach((brick) => {
                j++;
                if (pointInWindow(brick.x, brick.y)) {
                    i++;
                    library.ctx.fillStyle = brick.colour;
                    library.ctx.fillRect(brick.x, brick.y, library.brickSize, library.brickSize);
                }
            });
        });
        window.debugElem && (window.debugElem.innerHTML = i + " / " + j + " / " + k);
    },
    getBrick: function (x, y) {
        const newBrickKey = ~~(x) + "," + ~~(y);
        const newChunkKey = util.round(x, library.chunkSize) + "," + util.round(y, library.chunkSize);
        return library.chunkMap[newChunkKey] ? library.chunkMap[newChunkKey].bricks[newBrickKey] : undefined;
    },
    deleteBrick: function (x, y) {
        const newBrickKey = ~~(x) + "," + ~~(y);
        library.databaseRef.child(newBrickKey).remove();
    },
    writeBrick: function (x, y, colour) {
        const newBrickKey = ~~(x) + "," + ~~(y);
        const newChunkKey = util.round(x, library.chunkSize) + "," + util.round(y, library.chunkSize);
        library.databaseRef.child(newBrickKey).set({
            x,
            y,
            colour,
            chunk: newChunkKey
        });
    }
}

function changeChunk() {

    //if camera has moved
    if (library.camera.pos != library.lastCameraPos) {
        library.lastCameraPos = {
            ...library.camera.pos
        };

        //find current chunk
        let chunkX = util.round(library.camera.pos.x * -1, library.chunkSize);
        let chunkY = util.round(library.camera.pos.y * -1, library.chunkSize);
        let currentChunkKey = chunkX + "," + chunkY;

        //if camera has moved to new chunk
        if (currentChunkKey != library.lastChunkKey) {
            library.lastChunkKey = currentChunkKey;

            //add chunkBatchLength*chunkBatchLength chunks
            for (let i = -chunkBatchLength; i < chunkBatchLength; i++) {
                const innerChunkKeyX = (chunkX + i * library.chunkSize);
                for (let j = -chunkBatchLength; j < chunkBatchLength; j++) {
                    currentChunkKey = innerChunkKeyX + "," + (chunkY + j * library.chunkSize);
                    dbListenersOn(currentChunkKey);
                }
            }
        }
    }

    //if there are too many chunks, mark furthes away chunks for deletion.
    const existingChunks = Object.keys(library.chunkMap);
    const tooManyChunks = existingChunks.length - chunkBatchTarget;
    if (tooManyChunks > 0) {
        for (let i = 0; i < tooManyChunks; i++) {
            const parts = existingChunks[i].split(",");
            if (util.distanceBetweenPoints(parts[0], parts[1], library.camera.pos.x, library.camera.pos.y) > chunkDistanceMax) {
                library.chunksToDelete[existingChunks[i]] = true;
            }
        }
    }

    //delete section of marked chunks
    for (let i = 0; i < chunkBatchLength; i++) {
        const targetChunk = Object.keys(library.chunksToDelete).pop();
        if (targetChunk && library.chunkMap[targetChunk]) {
            dbListenersOff(targetChunk);
        }
    }
}

function pointInWindow(x, y) {
    return x > library.camera.scale.left &&
        x < library.camera.scale.right &&
        y > library.camera.scale.top &&
        y < library.camera.scale.bottom;
}

function dbListenersOn(chunkKey) {
    if (library.chunkMap[chunkKey] &&
        library.chunkMap[chunkKey].child_added &&
        library.chunkMap[chunkKey].child_changed &&
        library.chunkMap[chunkKey].child_removed) {
        delete library.chunksToDelete[chunkKey];
        return;
    }
    const query = library.databaseRef.orderByChild("chunk").equalTo(chunkKey);
    if (!library.chunkMap[chunkKey]) {
        library.chunkMap[chunkKey] = {};
    }
    if (!library.chunkMap[chunkKey].bricks) {
        library.chunkMap[chunkKey].bricks = [];
    }
    if (!library.chunkMap[chunkKey].child_added) {
        library.chunkMap[chunkKey].child_added = query.on('child_added', function (snapshot) {
            library.chunkMap[chunkKey].bricks[snapshot.key] = snapshot.val();
        });
    }
    if (!library.chunkMap[chunkKey].child_changed) {
        library.chunkMap[chunkKey].child_changed = query.on('child_changed', function (snapshot) {
            library.chunkMap[chunkKey].bricks[snapshot.key] = snapshot.val();
        });
    }
    if (!library.chunkMap[chunkKey].child_removed) {
        library.chunkMap[chunkKey].child_removed = query.on('child_removed', function (snapshot) {
            delete library.chunkMap[chunkKey].bricks[snapshot.key];
        });
    }
}

function dbListenersOff(chunkKey) {
    delete library.chunksToDelete[chunkKey];
    if (!library.databaseRef) {
        return;
    }
    if (library.chunkMap[chunkKey].child_added) {
        library.databaseRef.off('child_added', library.chunkMap[chunkKey].child_added);
    }
    if (library.chunkMap[chunkKey].child_changed) {
        library.databaseRef.off('child_changed', library.chunkMap[chunkKey].child_changed);
    }
    if (library.chunkMap[chunkKey].child_removed) {
        library.databaseRef.off('child_removed', library.chunkMap[chunkKey].child_removed);
    }
    delete library.chunkMap[chunkKey];
}

export default library;